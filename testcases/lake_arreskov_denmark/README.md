## Lake Arreskov, Denmark, testcase 

This model setup allows simulating the GOTM-WET model for the shallow Lake Arreskov from 1990 to 2014.
We recommend to limit the model simulation period to 1990-2006 due to increased uncertainty in the catchment and lake monitoring programme. 

The model setup was used in the publication:
**Simulating shifting ecological states in a restored, shallow lake with multiple single-model ensembles: Lake Arreskov, Denmark**, Environmental Modelling and Software (in review)
by Andersen, T.K., Nielsen, A., Jeppesen, E., Bolding, K., Johansson, L., Søndergaard, M. and Trolle, D. 2022. 

For more information on the lake and model configuration, please see the publication above.
