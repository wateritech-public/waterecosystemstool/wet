## Testcases for WET model 

Several model set-ups are provided as testcases for WET. Each testcase is based on a scientific paper.

**Lake Arreskov, Denmark**

WET model set-up for Danish shallow Lake Arreskov. Model study focused on simulating shifting ecosystem states between phytoplankton- and macrophyte dominated states and is documented in

Andersen, T. K., Nielsen, A., Jeppesen, E., Bolding, K., Johansson, L. S., Søndergaard, M., & Trolle, D. (2022). Simulating shifting ecological states in a restored, shallow lake with multiple single-model ensembles: Lake Arreskov, Denmark. [Environmental Modelling and Software](https://doi.org/10.1016/j.envsoft.2022.105501), 156, 105501. 

**Lake Bryrup, Denmark**
Model study investigating the effect of heat waves and warmer weather on water quality for Danish shallow Lake Bryrup. 

Chen, W., Nielsen, A., Andersen, T. K., Hu, F., Chou, Q., Søndergaard, M., Jeppesen, E., & Trolle, D. (2020). Modeling the ecological response of a temporarily summer-stratified lake to extreme heatwaves. [Water](https://doi.org/10.3390/w12010094), 12(1), 94. 

**Lake Bryrup, OBM**

Based on the study by Chen et al. (2020), an idealized lake model was configured to test and explored algorithms of fish food selection and feeding vs predator avoidance (i.e. optimal behaviour model, OBM).

Schnedler-Meyer, N. and Andersen, T.K. (under review) Dining in danger: Resolving adaptive fish behavior increases realism of modeled ecosystem dynamics

**QWET templates**

Three fabm.yaml files for each template choice in QWET. For description of QWET:

Nielsen, A., Schmidt Hu, F. R., Schnedler-Meyer, N. A., Bolding, K., Andersen, T. K., & Trolle, D. (2021). Introducing QWET – A QGIS-plugin for application, evaluation and experimentation with the WET model: Environmental Modelling and Software. [Environmental Modelling and Software](https://doi.org/10.1016/j.envsoft.2020.104886), 135, 104886. 

**Default files**

Default fabm.yaml and gotm.yaml files for standard model configuration.
